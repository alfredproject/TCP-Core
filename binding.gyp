{
"target_defaults":
    {
        "cflags" : ["-Wall", "-Wextra", "-Wno-unused-parameter"],
        "defines": [ "V8_DEPRECATION_WARNINGS=1" ],
        "include_dirs": [
        	"<!(node -e \"require('nan')\")",
        	"<!(node -e \"require('streaming-worker-sdk')\")"
    	],
    },
  "targets": [
    {
      "target_name": "server",
      "sources": [
      	"src/Server.cpp",
      	"src/Client.cpp"
      ]
    }
  ]
}