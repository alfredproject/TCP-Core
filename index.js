"use strict";
const path = require('path');
const worker = require("streaming-worker");
const m = require('./build/Release/obj.target/server.node');
const addon_path = path.join(__dirname, "./build/Release/server");
const server = worker(addon_path, {'port': 12999, 'maxConnections': 0});

/*server.from.on('board', function(val) {
	console.log(val);
});

server.from.on('connection', function(val) {
	console.log(val + " has connected");
});

server.from.on('close', () => {console.log("finished")})*/

module.exports = server;