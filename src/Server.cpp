#include "Server.h"
#include <chrono>
using namespace std;

Server::Server(Callback *data, Callback *complete, Callback *error_callback, v8::Local<v8::Object> & options) 
	: StreamingWorker(data, complete, error_callback) {
		if (options->IsObject() ) {
			v8::Local<v8::Value> _port = options->Get(New<v8::String>("port").ToLocalChecked());
			if (_port->IsNumber() ) {
				port = _port->NumberValue();
			}

			v8::Local<v8::Value> _max_connections = options->Get(New<v8::String>("maxConnections").ToLocalChecked());
			if (_max_connections->IsNumber()) {
				max_connections = _max_connections->NumberValue();
			}
		}
		current_connections = 0;
}

void Server::send_data(json data) {
	//Message _msg("data", data);
	//writeToNode(prog, _msg);
	q.push(data);
}

void Server::Execute(const AsyncProgressWorker::ExecutionProgress& progress) {
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port);
	bind(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress));
	listen(sockfd, 5);

	std::thread accept_thread(&Server::accept_connection, this);
	accept_thread.detach();	
	std::thread from_node(&Server::receive_from_node, this);
	from_node.detach();	
	while(true) {
		while (!q.empty()) {
			Message _msg(q.front()["type"], q.front().dump());
			writeToNode(progress, _msg);
			q.pop();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
}

void* Server::accept_connection() {
	while(true) {
		if (++current_connections < max_connections || max_connections == 0) {
			struct sockaddr_in client_address;
			socklen_t sosize  = sizeof(client_address);
			int client_socket = accept(sockfd, (struct sockaddr *) &client_address, &sosize);
			if (client_socket > 0) {
				Client * client = new Client(client_socket, this);
				//clients.push_back(*client);
				std::thread t(&Client::run, *client);
				t.detach();
			}
		}
	}
}

void* Server::receive_from_node() {
	while(true) {
		Message m = fromNode.read();
		if (m.name == "execute") {
			json data = json::parse(m.data);
			auto socket_string = data["socket"].get<int>();
			int socket = socket_string;
			std::string action = data["action"].get<std::string>();
			std::string msg = "execute:";
			msg += action;
			const char *msg_ready = msg.c_str();

			send(socket, msg_ready, strlen(msg_ready), 0);
		}
	}
}

StreamingWorker * create_worker(Callback *data, Callback *complete, Callback *error_callback, v8::Local<v8::Object> & options) {
	return new Server(data, complete, error_callback, options);
}

NODE_MODULE(server, StreamWorkerWrapper::Init)