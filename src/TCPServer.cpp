#include "TCPServer.h"

Nan::Persistent<v8::FunctionTemplate> TCPServer::constructor;

NAN_MODULE_INIT(TCPServer::Init) {
	v8::Local<v8::FunctionTemplate> ctor = Nan::New<v8::FunctionTemplate>(TCPServer::New);
	constructor.Reset(ctor);
	ctor->InstanceTemplate()->SetInternalFieldCount(1);
	ctor->SetClassName(Nan::New("TCPServer").ToLocalChecked());

	// link our getters and setter to the object property
	//Nan::SetAccessor(ctor->InstanceTemplate(), Nan::New("x").ToLocalChecked(), Vector::HandleGetters, Vector::HandleSetters);
	//Nan::SetAccessor(ctor->InstanceTemplate(), Nan::New("y").ToLocalChecked(), Vector::HandleGetters, Vector::HandleSetters);
	//Nan::SetAccessor(ctor->InstanceTemplate(), Nan::New("z").ToLocalChecked(), Vector::HandleGetters, Vector::HandleSetters);

	Nan::SetPrototypeMethod(ctor, "Send", Send);
	Nan::SetPrototypeMethod(ctor, "GetMessage", GetMessage);

	target->Set(Nan::New("TCPServer").ToLocalChecked(), ctor->GetFunction());
}

NAN_METHOD(TCPServer::New) {
	// Throw an error if constructor is called without new keyword
	if(!info.IsConstructCall()) {
	return Nan::ThrowError(Nan::New("Server::New - called without new keyword").ToLocalChecked());
	}

	// Expect exactly 2 arguments
	if(info.Length() != 3) {
	return Nan::ThrowError(Nan::New("Server::New - expected arguments port, maxConnections, listener").ToLocalChecked());
	}

	// Expect arguments to be numbers
	if(!info[0]->IsNumber() || !info[1]->IsNumber() || !info[2]->IsFunction()) {
	return Nan::ThrowError(Nan::New("Server::New - expected arguments to be number, number, function").ToLocalChecked());
	}

	// Create a new instance and wrap our javascript instance
	TCPServer* server = new TCPServer();
	server->Wrap(info.Holder());

	// Initialize it's values
	server->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	memset(&server->serverAddress, 0, sizeof(server->serverAddress));
	server->serverAddress.sin_family = AF_INET;
	server->serverAddress.sin_addr.s_addr=htonl(INADDR_ANY);
	server->serverAddress.sin_port=htons(info[0]->NumberValue());
	bind(server->sockfd,(struct sockaddr *)&server->serverAddress, sizeof(server->serverAddress));
	listen(server->sockfd,5);

	// Return the wrapped javascript instance
	//info.GetReturnValue().Set(info.Holder());
	Nan::Callback *progress = new Nan::Callback(Nan::To<v8::Function>(info[2]).ToLocalChecked());
	Nan::AsyncQueueWorker(new AsyncProgressWorkerStream(progress, progress, progress));

}

NAN_METHOD(TCPServer::Send) {
	cout << "Method not implemented";
}

NAN_METHOD(TCPServer::GetMessage) {
	cout << "Method not implemented";
}