#ifndef SERVER_H
#define SERVER_H

#include <nan.h>

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <thread>
#include <queue>

#include "Client.h"
#include "streaming-worker.h"
#include "json.hpp"

class Client;

#define MAX_PACKET_SIZE 4096

using namespace std;
using json = nlohmann::json;

class Server : public StreamingWorker {
	public:
		int sockfd, newsockfd, n, pid;
		struct sockaddr_in serverAddress;
		struct sockaddr_in clientAddress;
		pthread_t serverThread;
		char msg[MAX_PACKET_SIZE];
		queue <json> q;
		//static string Message;

		void* receive_from_node();
		void* accept_connection();
		void send_data(json);

		Server(Callback *data, Callback *complete, Callback *error_callback, v8::Local<v8::Object> & options);
		void Execute(const AsyncProgressWorker::ExecutionProgress& progress);

	private:
		short port;
		int max_connections;
		int current_connections;
		std::vector<Client> clients;
};

#endif