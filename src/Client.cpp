#include "Client.h"

using namespace std;

Client::Client(int socket, Server * server_) : server(server_) {
	this->socket = socket;
	module_name.clear();
}

void* Client::run() {
	write("getName \n");

	while(module_name.empty()) { 
	    std::string name = read_(); 
	    if (!name.empty()) { 
	      module_name = name; 
	      json jsonObject;
		  jsonObject["type"] = "connection";
		  json jsonData;
		  jsonData["socket"] = this->socket;
		  jsonData["name"] = name;
		  jsonObject["data"] = jsonData;

		  this->server->send_data(jsonObject);
	    }
	} 

	write("initialize \n");
	std::string in_data = std::string("");

	std::thread ping_thread(&Client::ping, this);
	ping_thread.detach();	

	while(true) {
		in_data = read_();

		if (in_data.empty()) break;

		json jsonObject;
		jsonObject["type"] = "board";
		jsonObject["board"] = module_name;
		jsonObject["data"] = in_data;

		this->server->send_data(jsonObject);
	}

	return 0;
}

void* Client::ping() {
	while (true) {
		write("ping");
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	}
}

void Client::write(std::string message) {
	const char *msg = message.c_str();
	send(socket, msg, strlen(msg), 0);
}

std::string Client::read_() {
	char buffer[MAX_PACKET_SIZE] = {0};
	int read_val = read(socket, buffer, (size_t)MAX_PACKET_SIZE);

	if (read_val < 1) {
		// Todo: Disconnection logic here!
		json jsonObject;
		jsonObject["type"] = "disconnection";
		json jsonData;
		jsonData["socket"] = this->socket;
		jsonData["name"] = module_name;
		jsonObject["data"] = jsonData;
		this->server->send_data(jsonObject);
	}

	return std::string(buffer);
}

int Client::getSocket() {
	return socket;
}

std::string Client::getModuleName() {
	return module_name;
}