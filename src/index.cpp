//
// Created by juan on 4/28/18.
//

#include <nan.h>
#include "TCPServer.h"
#include "AsyncProgressWorkerStream.h"
// Todo: Include the headers of all the modules to initialize

NAN_MODULE_INIT(InitModule) {
        /**
         * Todo: Initialize all the modules
         * Ex: ModuleName::Init(target);
         */
	//TCPServer::Init(target);
	AsyncProgressWorkerStream::Init(target);
}

NODE_MODULE(alfredtcpcore, InitModule);
