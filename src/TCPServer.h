#include <nan.h>

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "AsyncProgressWorkerStream.h"

using namespace std;

#define MAX_PACKET_SIZE 4096

class TCPServer : Nan::ObjectWrap {

    public:
    int sockfd, newsockfd, n, pid;
    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddress;
    pthread_t serverThread;
    char msg[MAX_PACKET_SIZE];
    static string Message;

    //void setup(int port);
    string receive();
    //string getMessage();
    //void Send(string msg);
    void detach();
    void clean();

    static NAN_MODULE_INIT(Init);
    static NAN_METHOD(New);
    static NAN_METHOD(Send);
    static NAN_METHOD(GetMessage);

    static NAN_GETTER(HandleGetters);
    static NAN_SETTER(HandleSetters);

    static Nan::Persistent<v8::FunctionTemplate> constructor;

    private:
    static void * Task(void* argv);
};