#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <thread>
#include "Server.h"
#include "json.hpp"

class Server;

#define MAX_PACKET_SIZE 4096

using namespace std;
using json = nlohmann::json;

class Client {
	public:
		Client(int socket, Server * server);
		void* run();
		void write(std::string message);
		std::string read_();
		int getSocket();
		std::string getModuleName();
		void* ping();

	private:
		int socket;
		std::string module_name;
		Server * server;
};

#endif