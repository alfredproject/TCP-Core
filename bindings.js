var alfredtcp;

if (process.env.DEBUG)
	alfredtcp = require('./build/Debug/alfredtcpcore.node')
else
	alfredtcp = require('./build/Release/alfredtcpcore.node')

module.exports = alfredtcp;